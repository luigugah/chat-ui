(function() {
  'use strict';

  angular
    .module('chat')
    .controller('ChatController', ChatController);

  /** @ngInject */
  function ChatController($stateParams, chatAPI) {
    var vm = this;

    activate();

    function activate() {
      chatAPI.history($stateParams.user)
        .then(function (chat) {
          vm.chat = chat;
        });
    }

  }
})();
